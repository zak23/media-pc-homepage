var ajaxload = '<div class="ajaxloading">&nbsp;</div>';

function switchstatus(item, status) {
	item = parseInt(item);
	status = parseInt(status);
	if(status == 1) {
		replacement = '<a href="" onclick="switchstatus(' + item + ', 0);return false;" class="checkbox_marked">.</a>';
	} else {
		replacement = '<a href="" onclick="switchstatus(' + item + ', 1);return false;" class="checkbox_off">.</a>';
	};
	$("#item"+item).html(replacement);
	s_send = $.get("/", {"cs":"ajax","m":"schedule","item":item,"status":status});
};

function rundelete(id) {
	$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
	$.get("/", {"cs":"ajax","m":"shows","func":"delete","show":parseInt(id)}, function(data){
		$("#inner_shows").css("height", "auto").html(data);
	});
};

function showopts(id) {
	$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
	$.get("/", {"cs":"ajax","m":"opts","show":parseInt(id)}, function(data){
		$("#inner_shows").css("height", "auto").html(data);
	});
};

function saveopts(id) {
	hashd = parseInt($("#hashd").attr("value"));
	hasproper = parseInt($("#hasproper").attr("value"));
	if($("#pushopts").length > 0) {
		pushopts = parseInt($("#pushopts").attr("value"));
		$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
		$.get("/", {"cs":"ajax","m":"shows","func":"opts","show":parseInt(id),"hashd":hashd,"hasproper":hasproper,"pushopts":pushopts}, function(data){
			$("#inner_shows").css("height", "auto").html(data);
		});
	} else {
		$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
		$.get("/", {"cs":"ajax","m":"shows","func":"opts","show":parseInt(id),"hashd":hashd,"hasproper":hasproper}, function(data){
			$("#inner_shows").css("height", "auto").html(data);
		});
	};
};

function updatetwitterpref(kind) {
	if(kind == 'seen') {
		if($("#seen").is(":checked")) {
			value = 1;
		} else {
			value = 0;
		}
	} else {
		return false;
	}
	$.get("/", {"cs": "ajax", "m": "twitter", "kind": kind, "value": value});
};

function cancelopts() {
	$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
	$.get("/", {"cs":"ajax","m":"shows"}, function(data){
		$("#inner_shows").css("height", "auto").html(data);
	});
	return false;
};

function addshow() {
	id = parseInt($("#show_selection").attr("value"));
	$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
	$.get("/", {"cs":"ajax","m":"shows","func":"add","show":id}, function(data){
		$("#inner_shows").css("height", "auto").html(data);
	});
};

function searchaddshow() {
	id = parseInt($("#search_show_selection").attr("value"));
	query = $("#showsearch").attr("value");
	$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
	$.get("/", {"cs":"ajax","m":"shows","func":"add","show":id,"query":query}, function(data){
		$("#inner_shows").css("height", "auto").html(data);
		document.getElementById("showsearch").focus();
	});
};

function togglesearch() {
	div = '#show_search';
	if($(div).is(":visible")) {
		$("#searchbutton").attr("value", "Search");
		$(div).slideUp("fast");
	} else {
		$(div).slideDown("fast");
		$("#searchbutton").attr("value", "Toggle");
	};
};

function runsearch() {
	query = $("#showsearch").attr("value");
	$("#inner_shows").css("height", $("#inner_shows").height()+"px").html(ajaxload);
	$.get("/", {"cs":"ajax","m":"shows","query":query}, function(data){
		$("#inner_shows").css("height", "auto").html(data);
	});
};